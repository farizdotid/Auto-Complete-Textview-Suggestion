package com.farizdotid.belajarautocomplete;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private AutoCompleteTextView actext_namakota;
    private AutoCompleteTextView actext_namakotadb;
    String[] namakota = {"Bandung", "Jakarta", "Surabaya", "Malang", "Purwakarta", "Purwokerto"};
    private DBHelperNamaProvinsi dbHelperNamaProvinsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelperNamaProvinsi = new DBHelperNamaProvinsi(this);
        dbHelperNamaProvinsi.loadContent();

        initAutoCompleteNamaProv();
        initAutoCompleteNamaProv2();
    }

    private void initAutoCompleteNamaProv(){
        actext_namakota = (AutoCompleteTextView) findViewById(R.id.actext_namakota);

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, namakota);
        actext_namakota.setAdapter(adapter);
        actext_namakota.setThreshold(1);

        actext_namakota.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, (CharSequence) parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void initAutoCompleteNamaProv2(){
        actext_namakotadb = (AutoCompleteTextView) findViewById(R.id.actext_namakotadb);

        final String[] namaKotaDB = dbHelperNamaProvinsi.SelectAllDataProv();
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, namaKotaDB);
        actext_namakotadb.setAdapter(adapter);
        actext_namakotadb.setThreshold(1);
        actext_namakotadb.dismissDropDown();

        actext_namakotadb.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemClick: FROM DB > " + parent.getItemAtPosition(position));
                String namakota = (String) parent.getItemAtPosition(position);
                dbHelperNamaProvinsi.searchIDByName(namakota);
            }
        });
    }
}
